|pipeline| |coverage| |rtd|

.. |pipeline| image:: https://framagit.org/1ohmatr/sw/py/nztests/badges/master/pipeline.svg

.. |coverage| image:: https://framagit.org/1ohmatr/sw/py/nztests/badges/master/coverage.svg

.. |rtd| image:: https://readthedocs.org/projects/nztests/badge/?version=latest

Template for python packages
