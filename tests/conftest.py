import pytest,random

def random_list(seed,size):
    state = random.getstate()
    random.seed(seed)
    l = list(random.getrandbits(20) for _ in range(size))
    random.setstate(state)
    return l

@pytest.fixture(params=random_list(14759,100))
def seed(request):
    return request.param
