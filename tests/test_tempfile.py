from context import package as p
# from contextlib import redirect_stdout
# from io import StringIO
import pytest

def test_tempfile_wrapper():
    tf = p.tempfile.TemporaryFileWrapper()
    assert(tf.write('hello') == 5)
    tf.flush()
    tf.seek(0)
    assert(tf.read() == 'hello')
    tf.close()
    assert(tf.file.closed)

def test_tempfile_double_wrapper():
    tf = p.tempfile.TemporaryFileWrapper()
    tf2 = p.tempfile.TemporaryFileWrapper(tf.file)
    tf.close()
    assert(not tf.closed)
    tf.close()
    assert(not tf.closed)
    tf2.close()
    assert(tf.closed)
