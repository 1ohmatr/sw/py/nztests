try:
    from .__version__ import VERSION
except:               # pragma: no cover
    VERSION='unknown'

from . import tempfile
